import { getProxy } from "detox-jest-spy/dist/client";


const spyProxy = getProxy("Analytics2")

const oldFetch = global.fetch

const proxy =((...filters)=>new Proxy({},{
    get :  (target,props) =>{

        return (...args)=>{
            if(filters.some(f=>f(args[0]))){
                spyProxy[props](...args)

            }
            
            return oldFetch(...args)
        }
    }

}))(x=>x.indexOf("/track")!=x.length-6,url=>url.indexOf("ACCESS-TOKEN")>0)
global.fetch = (...args)=>proxy.fetch(...args)

export default spyProxy;