import { start, stop, getSpy } from "detox-jest-spy";
// detox overrides the global expect variable with it's own implementation, so we need to reimport it under another name
import jestExpect from 'expect';
describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  beforeAll(() => {
    start(); // start the detox-jest-spy server
  });

  afterAll(() => {
    stop(); // stop the server after test run is complete
  });

  it('should have welcome screen', async () => {
    await expect(element(by.id('welcomeButton'))).toBeVisible();
  });

  it('should show hello screen after tap', async () => {
    await element(by.id('welcomeButton')).tap();
    const spy = getSpy("Analytics2.fetch");
    jestExpect(spy).toBeCalled();
    jestExpect(spy).toBeCalledWith("https://api.instagram.com/v1/users/self/media/recent/?access_token=ACCESS-TOKEN");

  });
});
